﻿def change(A,x,y):
    A[x-1],A[y-1] = A[y-1],A[x-1]
    return A

Mat=[[1,1,1,1,1],
     [2,2,2,2,2],
     [3,3,3,3,3],
     [4,4,4,4,4]]
print('Матриця: ')
for k in Mat:
    print(k)
i=1
j=3
print('\n')
Mat=change(Mat,i,j)
print('Перетворена матриця:')
for k in Mat:
    print(k)